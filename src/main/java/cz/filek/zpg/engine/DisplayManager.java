package cz.filek.zpg.engine;

import cz.filek.zpg.config.Config;
import cz.filek.zpg.input.KeyboardHandler;
import cz.filek.zpg.input.MouseButtonHandler;
import cz.filek.zpg.input.MouseCursorHandler;
import cz.filek.zpg.input.MouseScrollHandler;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryUtil;

import static org.lwjgl.glfw.GLFW.*;

/**
 * @author Filip Jani - 1. 11. 2016.
 *         Třída zajišťující vytváření GLFW okna a synchronizaci snímků za sekundu
 */
public class DisplayManager {
    // ID vytvořeného okna
    private static long windowID;
    // Proměnné používané pro nastavení FPS
    private static long variableYieldTime;
    private static long lastTime;
    private static float delta;
    private static long lastTimeFrame;


    /**
     * Vytváří GLFW okno
     *
     * @return long window ID
     */
    public static long createDisplay() {
        boolean glfwInit = glfwInit();

        if (!glfwInit) {
            System.err.println("Chyba při inicializaci GLFW");
        }
        windowID = glfwCreateWindow(Config.APP_WIDTH, Config.APP_HEIGHT, "ZPG", MemoryUtil.NULL, MemoryUtil.NULL);
        if (windowID == MemoryUtil.NULL) {
            System.err.println("Chyba při vytváření okna");
        }

        glfwMakeContextCurrent(windowID);
        glfwSwapInterval(0);
        glfwShowWindow(windowID);

        GLFWKeyCallback keyCallback = new KeyboardHandler();
        GLFWMouseButtonCallback mouseButtonCallback = new MouseButtonHandler();
        MouseScrollHandler mouseScrollHandler = new MouseScrollHandler(windowID);
        MouseCursorHandler mouseCursorHandler = new MouseCursorHandler(windowID);

        glfwSetKeyCallback(windowID, keyCallback);
        glfwSetMouseButtonCallback(windowID, mouseButtonCallback);

        glfwSetCursorPos(windowID, Config.APP_WIDTH / 2, Config.APP_HEIGHT / 2);

        GL.createCapabilities();
        lastTimeFrame = getCurrentTime();

        return windowID;
    }

    /**
     * Synchronizuje snímkovací frekvenci na dané FPS
     *
     * @param fps int
     */
    public static void sync(int fps) {
        if (fps <= 0) return;

        long sleepTime = 1000000000 / fps; // nanoseconds to sleep this frame
        // yieldTime + remainder micro & nano seconds if smaller than sleepTime
        long yieldTime = Math.min(sleepTime, variableYieldTime + sleepTime % (1000 * 1000));
        long overSleep = 0; // time the sync goes over by

        try {
            while (true) {
                long t = System.nanoTime() - lastTime;

                if (t < sleepTime - yieldTime) {
                    Thread.sleep(1);
                } else if (t < sleepTime) {
                    // burn the last few CPU cycles to ensure accuracy
                    Thread.yield();
                } else {
                    overSleep = t - sleepTime;
                    break; // exit while loop
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lastTime = System.nanoTime() - Math.min(overSleep, sleepTime);

            // auto tune the time sync should yield
            if (overSleep > variableYieldTime) {
                // increase by 200 microseconds (1/5 a ms)
                variableYieldTime = Math.min(variableYieldTime + 200 * 1000, sleepTime);
            } else if (overSleep < variableYieldTime - 200 * 1000) {
                // decrease by 2 microseconds
                variableYieldTime = Math.max(variableYieldTime - 2 * 1000, 0);
            }
        }
        long currentFrameTime = getCurrentTime();
        delta = (currentFrameTime - lastTimeFrame) / 1000f;
        lastTimeFrame = currentFrameTime;
    }

    public static float getFrameTimeSeconds() {
        return delta;
    }

    private static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static long getWindowID() {
        return windowID;
    }
}
