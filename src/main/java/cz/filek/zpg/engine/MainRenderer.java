package cz.filek.zpg.engine;

import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.entities.Entity;
import cz.filek.zpg.entities.Light;
import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Matrix4f;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.TexturedModel;
import cz.filek.zpg.shaders.StaticShader;
import cz.filek.zpg.shaders.TerrainShader;
import cz.filek.zpg.terrains.Terrain;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

/**
 * Hlavní renderovací třída
 * - slouží k renderování celé scény
 */
public class MainRenderer {
    private Matrix4f projectionMatrix;
    // Konstanty určené pro výpočet view matice
    private final float FAR_PLANE = 1000f;
    private final float NEAR_PLANE = 1f;
    private final float FOV = 70;
    // ########################################

    // Pozadí scény
    private final Vector3f BG_COLOR = new Vector3f(0.85f, 0.75f, 0.52f);

    // Instance shaderů a rendererů pro dané objekty
    private StaticShader shader = new StaticShader();
    private EntityRenderer entityRenderer;

    private TerrainShader terrainShader = new TerrainShader();
    private TerrainRenderer terrainRenderer;

    private SkyboxRenderer skyboxRenderer;
    // ########################################

    // Mapa entit - textured model
    private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
    // List všech terénů
    private List<Terrain> terrains = new ArrayList<Terrain>();


    /**
     * Konstruktor
     * zapne culling - nevykresluje vnitřní strany modelů
     * vytvoří projekční matici
     * vytvoří instance jednotlivých rendererů
     *
     * @param loader loader
     */
    public MainRenderer(Loader loader) {
        enableCulling();
        projectionMatrix = Maths.createProjectionMatrix(FAR_PLANE, NEAR_PLANE, FOV);
        entityRenderer = new EntityRenderer(shader, projectionMatrix);
        terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
        skyboxRenderer = new SkyboxRenderer(loader, projectionMatrix);
    }

    /**
     * Metoda zajišťující vykreslení všech objektů
     *
     * @param light  instance světla
     * @param camera instance kamery
     */
    public void render(Light light, Camera camera) {
        prepare();
        shader.start();
        shader.loadLight(light);
        shader.loadViewMatrix(camera);
        shader.loadSkyColor(BG_COLOR.x, BG_COLOR.y, BG_COLOR.z);
        entityRenderer.render(entities);
        shader.stop();

        terrainShader.start();
        terrainShader.loadLight(light);
        terrainShader.loadViewMatrix(camera);
        terrainShader.loadSkyColor(BG_COLOR.x, BG_COLOR.y, BG_COLOR.z);
        terrainRenderer.render(terrains);
        terrainShader.stop();

        skyboxRenderer.render(camera);

        entities.clear();
        terrains.clear();
    }

    /**
     * Vytváření mapy entita - textured model
     *
     * @param entity Entity
     */
    public void processEntity(Entity entity) {
        TexturedModel texturedModel = entity.getTexturedModel();
        List<Entity> batch = entities.get(texturedModel);
        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<Entity>();
            newBatch.add(entity);
            entities.put(texturedModel, newBatch);
        }
    }

    /**
     * Přidání terénu do listu
     *
     * @param terrain Terrain
     */
    public void processTerrain(Terrain terrain) {
        terrains.add(terrain);
    }

    /**
     * Zapne Culling
     */
    public static void enableCulling() {
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    }

    /**
     * Vypne Culling
     */
    public static void disableCulling() {
        glDisable(GL_CULL_FACE);
    }

    /**
     * Vyčišténí scény před renderováním
     */
    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(BG_COLOR.x, BG_COLOR.y, BG_COLOR.z, 1);
    }

    /**
     * Vypnutí shaderů
     */
    public void cleanUp() {
        shader.cleanUp();
        terrainShader.cleanUp();
    }

    /**
     * Vrací projekční matici
     *
     * @return Matrix4f
     */
    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }
}
