package cz.filek.zpg.engine;

import cz.filek.zpg.entities.Entity;
import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Matrix4f;
import cz.filek.zpg.models.RawModel;
import cz.filek.zpg.models.TexturedModel;
import cz.filek.zpg.shaders.StaticShader;
import cz.filek.zpg.textures.ModelTexture;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.util.List;
import java.util.Map;


/**
 * @author Filip Jani - 1. 11. 2016.
 *         Třída zajišťující vykreslování objektů typu Entity
 */
public class EntityRenderer {
    private StaticShader staticShader;

    /**
     * Konstruktor příjmající StaticShader, který je používaný pro vykreslování entit
     *
     * @param shader           - Static shader
     * @param projectionMatrix - projection matrixx
     */
    public EntityRenderer(StaticShader shader, Matrix4f projectionMatrix) {
        this.staticShader = shader;
        shader.start();
        shader.loadProjMatrix(projectionMatrix);
        shader.stop();

    }

    /**
     * Metoda zajišťující vykreslení dané entity
     *
     * @param entities mapa - protože můžeme mít více entit se stejným 3D modelem
     */
    public void render(Map<TexturedModel, List<Entity>> entities) {
        for (TexturedModel texturedModel : entities.keySet()) {
            prepareTexturedModel(texturedModel);
            List<Entity> batch = entities.get(texturedModel);
            for (Entity entity : batch) {
                prepareInstance(entity);
                GL11.glDrawElements(GL11.GL_TRIANGLES, texturedModel.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
            }
            unbindTexturedModel();
        }
    }

    /**
     * Připraví model k vykreslení
     *
     * @param texturedModel otexturovaný model
     */
    private void prepareTexturedModel(TexturedModel texturedModel) {
        RawModel rawModel = texturedModel.getRawModel();
        GL30.glBindVertexArray(rawModel.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);
        ModelTexture texture = texturedModel.getModelTexture();
        if (texture.hasTransparency()) {
            MainRenderer.disableCulling();
        }
        staticShader.loadFakeLighting(texture.useFakeLighting());
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getModelTexture().getID());
    }

    /**
     * Po vykreslení restartuje openGL atributy
     */
    private void unbindTexturedModel() {
        MainRenderer.enableCulling();
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
    }

    /**
     * Nahraje každé entitě transformační matici, aby byla entita vykreslena na správné místo
     *
     * @param entity Entita
     */
    private void prepareInstance(Entity entity) {
        Matrix4f tansMatrix = Maths.createTransMatrix(entity.getPosition(), entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());
        staticShader.loadTransmatrix(tansMatrix);
    }
}
