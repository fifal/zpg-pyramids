package cz.filek.zpg.engine;

import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.math.Matrix4f;
import cz.filek.zpg.models.RawModel;
import cz.filek.zpg.shaders.SkyboxShader;
import org.lwjgl.opengl.*;


/**
 * @author Filip Jani - 22. 11. 2016.
 *         <p>
 *         Třída zajišťující vykreslení skyboxu
 */
public class SkyboxRenderer {
    // Poloměr skyboxu
    private static final float SIZE = 1024f;

    // Pole vrcholů krychle
    private static final float[] VERTICES = {
            -SIZE, SIZE, -SIZE,
            -SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, SIZE, -SIZE,
            -SIZE, SIZE, -SIZE,

            -SIZE, -SIZE, SIZE,
            -SIZE, -SIZE, -SIZE,
            -SIZE, SIZE, -SIZE,
            -SIZE, SIZE, -SIZE,
            -SIZE, SIZE, SIZE,
            -SIZE, -SIZE, SIZE,

            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,

            -SIZE, -SIZE, SIZE,
            -SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, -SIZE, SIZE,
            -SIZE, -SIZE, SIZE,

            -SIZE, SIZE, -SIZE,
            SIZE, SIZE, -SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            -SIZE, SIZE, SIZE,
            -SIZE, SIZE, -SIZE,

            -SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE, SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE, SIZE,
            SIZE, -SIZE, SIZE
    };

    // Pole názvů textur pro daný skybox
    private static String[] TEXTURE_FILES = {"skybox/right", "skybox/left", "skybox/up", "skybox/down", "skybox/back", "skybox/front"};

    private RawModel cube;
    private int texture;
    private SkyboxShader skyboxShader;

    /**
     * Konstruktor - vytvoří RawModel krychle - nahraje do VAO
     * - získání ID textury skyboxu
     *
     * @param loader           Loader
     * @param projectionMatrix Matrix4f
     */
    public SkyboxRenderer(Loader loader, Matrix4f projectionMatrix) {
        cube = loader.loadToVAO(VERTICES, 3);
        texture = loader.loadSkyBox(TEXTURE_FILES);
        skyboxShader = new SkyboxShader();
        skyboxShader.start();
        skyboxShader.loadProjectionMatrix(projectionMatrix);
        skyboxShader.stop();
    }

    /**
     * Vyrenderování skyboxu
     * - Vykreslení pole vrcholů cube a nabindování textury na krychli
     *
     * @param camera Camera - kvůli pozici
     */
    public void render(Camera camera) {
        skyboxShader.start();
        skyboxShader.loadViewMatrix(camera);
        GL30.glBindVertexArray(cube.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texture);
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, cube.getVertexCount());
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
        skyboxShader.stop();
    }
}
