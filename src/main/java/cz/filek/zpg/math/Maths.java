package cz.filek.zpg.math;

import cz.filek.zpg.config.Config;
import cz.filek.zpg.entities.Camera;

/**
 * @author Filip Jani - on 1. 11. 2016.
 *         <p>
 *         Třída pro vytváření transformačních matic
 *         -Obsahuje chyby, které se mi nepodařilo odhalit
 *         <p>
 *         Ostatní třídy v tomto package jsou zkopírované ze zdrojových souborů knihovny LWJGL verze 2, protože LWJGL verze 3
 *         tyto třídy neobsahuje.
 */
public class Maths {
    public static Matrix4f createTransMatrix(Vector3f translation, float rx, float ry, float rz, float scale) {
        Matrix4f matrix4f = new Matrix4f();
        Matrix4f translate = Matrix4f.translate(translation.x, translation.y, translation.z);
        matrix4f = matrix4f.multiply(translate);
        matrix4f = matrix4f.multiply(Matrix4f.rotate((float) Math.toRadians(rx), 1, 0, 0));
        matrix4f = matrix4f.multiply(Matrix4f.rotate((float) Math.toRadians(ry), 0, 1, 0));
        matrix4f = matrix4f.multiply(Matrix4f.rotate((float) Math.toRadians(rz), 0, 0, 1));
        matrix4f = matrix4f.multiply(matrix4f.scale(scale, scale, scale));
        return matrix4f;
    }

    public static Matrix4f createProjectionMatrix(float farPlane, float nearPlane, float FOV) {
        float aspectRatio = (float) Config.APP_WIDTH / (float) Config.APP_HEIGHT;
        float yScale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))) * aspectRatio);
        float xScale = yScale / aspectRatio;
        float frustumLength = farPlane - nearPlane;

        Matrix4f projection = new Matrix4f();
        projection.m00 = xScale;
        projection.m11 = yScale;
        projection.m22 = -((farPlane + nearPlane) / frustumLength);
        projection.m23 = -1;
        projection.m32 = -((2 * nearPlane * farPlane) / frustumLength);
        projection.m33 = 0;

        return projection;
    }

    public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();
        viewMatrix = viewMatrix.multiply(Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), 1, 0, 0));
        viewMatrix = viewMatrix.multiply(Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), 0, 1, 0));
        Vector3f cameraPos = camera.getPosition();
        Vector3f negativeCameraPos = new Vector3f(-cameraPos.x, -cameraPos.y, -cameraPos.z);
        Matrix4f translate = Matrix4f.translate(negativeCameraPos.x, negativeCameraPos.y, negativeCameraPos.z);
        viewMatrix = viewMatrix.multiply(translate);
        return viewMatrix;
    }

    public static float barryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
        float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
        float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
        float l3 = 1.0f - l1 - l2;
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;
    }


}
