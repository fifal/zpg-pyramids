package cz.filek.zpg.input;

import org.lwjgl.glfw.GLFWScrollCallback;

import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;

/**
 * @author Filip Jani - 7. 11. 2016.
 *         <p>
 *         Třída zachytáváající pohyb kolečka myši
 */
public class MouseScrollHandler {
    private static long windowID;

    private static double xOffset;
    private static double yOffset;

    public MouseScrollHandler(long windowID) {
        this.windowID = windowID;
    }

    /**
     * Vrací rozdíl oproti předchozí pozici kolečka myši v ose X
     *
     * @return double
     */
    public static double getDX() {
        glfwSetScrollCallback(windowID, new GLFWScrollCallback() {
            @Override
            public void invoke(long window, double xoffset, double yoffset) {
                xOffset = xoffset;
                yOffset = yoffset;
            }
        });
        double result = xOffset;
        xOffset = 0;
        return result;
    }

    /**
     * Vrací rozdíl oproti předchozí pozici kolečka myši v ose Y
     *
     * @return double
     */
    public static double getDY() {
        glfwSetScrollCallback(windowID, new GLFWScrollCallback() {
            @Override
            public void invoke(long window, double xoffset, double yoffset) {
                xOffset = xoffset;
                yOffset = yoffset;
            }
        });
        double result = yOffset;
        yOffset = 0;
        return result;
    }
}
