package cz.filek.zpg.input;

import org.lwjgl.glfw.GLFWKeyCallback;

import static org.lwjgl.glfw.GLFW.*;

/**
 * @author Filip Jani - 2. 11. 2016.
 *         <p>
 *         Třída zachytávající stisknuté klávesy na klávesnici
 */
public class KeyboardHandler extends GLFWKeyCallback {
    // Pole pro stisknuté klávesy
    private static boolean[] keys = new boolean[65536];

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
        keys[key] = action != GLFW_RELEASE;
    }

    /**
     * Zjištění jestli je daná klávesa stisknutá
     *
     * @param keycode GLFW kód klávesy
     * @return true pokud je stisknuta
     */
    public static boolean isKeyDown(int keycode) {
        return keys[keycode];
    }
}
