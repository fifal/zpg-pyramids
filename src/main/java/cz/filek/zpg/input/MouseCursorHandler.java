package cz.filek.zpg.input;

import cz.filek.zpg.math.Vector2f;
import org.lwjgl.BufferUtils;

import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;

/**
 * @author Filip Jani - 7. 11. 2016.
 *         <p>
 *         Třída pro zachytávání pohybu myši - cursoru
 */
public class MouseCursorHandler {
    private static long windowID;
    private static double xPos;
    private static double yPos;

    public MouseCursorHandler(long windowID) {
        this.windowID = windowID;
    }

    /**
     * Vrací pozici myši na obrazovce
     *
     * @return Vector2f
     */
    public static Vector2f getPosition() {
        DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
        DoubleBuffer y = BufferUtils.createDoubleBuffer(1);

        glfwGetCursorPos(windowID, x, y);
        x.rewind();
        y.rewind();

        xPos = x.get();
        yPos = y.get();

        return new Vector2f((float) xPos, (float) yPos);
    }

    /**
     * Vrací rozdíl oproti předchozí pozici při pohybu myši v ose X
     *
     * @return float
     */
    public static float getDX() {
        double xPosLast = xPos;
        double yPosLast = yPos;
        Vector2f pos = getPosition();

        return (float) (xPosLast - xPos);
    }

    /**
     * Vrací rozdíl oproti předchozí pozici při pohybu myši v ose Y
     *
     * @return float
     */
    public static float getDY() {
        double xPosLast = xPos;
        double yPosLast = yPos;
        Vector2f pos = getPosition();

        return (float) (yPosLast - yPos);
    }
}
