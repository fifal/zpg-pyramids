package cz.filek.zpg.entities;

import cz.filek.zpg.input.MouseButtonHandler;
import cz.filek.zpg.input.MouseCursorHandler;
import cz.filek.zpg.input.MouseScrollHandler;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.terrains.Terrain;


/**
 * @author Filip Jani - 2. 11. 2016.
 *         <p>
 *         Kamera uchovávající informace o kameře
 *         <p>
 *         Bohužel jsem musel použít magickou konstantu, protože mám chybu někde při počítání pohledových,
 *         transformačních a projekčních matic. Chybu se mi nepodařilo najít ale částečně se mi ji podařilo
 *         eliminovat pomocí magické konstanty.
 */
public class Camera {
    // Vzdálenost od hráče
    private float distanceFromPlayer = 20;
    // Otočení okolo hráče
    private float angleAroundPlayer = 0;
    // Magická konstanta
    private final float MAGIC_CONST = 58.5f;


    private Terrain terrain;

    // Pozice kamery ve světě
    private Vector3f position = new Vector3f(0, 0, 0);
    // Natočení kamery podle osy X
    private float pitch = 20;
    // Natočení kamery podle osy Y
    private float yaw;
    // Natočení kamery podle osy Z
    private float roll;

    private Player player;

    /**
     * Konstruktor
     *
     * @param terrain Terrain
     * @param player  Player
     */
    public Camera(Terrain terrain, Player player) {
        this.player = player;
        this.terrain = terrain;
    }

    /**
     * Posun kamery při pohybu hráče
     */
    public void move() {
        calculateZoom();
        calculatePitch();
        calculateAngleAroundPlayer();

        float horizontalDistance = calculateHorizontalDistance();
        float verticalDistance = calculateVerticalDistance();
        calculateCameraPosition(horizontalDistance, verticalDistance);
    }

    /**
     * Výpočet pozice kamery
     *
     * @param horizontalDistance horizontální vzdálenost kamery od hráče
     * @param verticalDistance   vertikální vzdálenost kamery od hráče
     */
    private void calculateCameraPosition(float horizontalDistance, float verticalDistance) {
        float theta = player.getRotY() / MAGIC_CONST + angleAroundPlayer;
        float offsetX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
        float offsetZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));

        // Nastavení pozice
        position.x = player.getPosition().x - offsetX;
        position.z = player.getPosition().z - offsetZ;
        position.y = player.getPosition().y + verticalDistance;

        // Kolize kamery s terénem - nechceme se dostat pod terén
        float height = terrain.getHeightOfTerrain(this.position.x, this.position.z);
        if (position.y < height) {
            position.y = height + 1f;
        }
        // Natočíme kameru okolo hráče
        yaw = 180 * MAGIC_CONST - player.getRotY() - angleAroundPlayer * MAGIC_CONST;
    }

    /**
     * Výpočet horizontální vzdálenosti od hráče pomocí úhlu natočení podle osy X a distanceFromPlayer
     *
     * @return float
     */
    private float calculateHorizontalDistance() {
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    /**
     * Výpočet vertikální vzdálenosti od hráče pomocí úhlu natočení podle osy X a distanceFromPlayer
     *
     * @return float
     */
    private float calculateVerticalDistance() {
        return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }

    /**
     * Upravení Zoomu, při pohybu kolečka myši
     */
    private void calculateZoom() {
        float zoomLevel = (float) MouseScrollHandler.getDY() * 0.5f;
        distanceFromPlayer -= zoomLevel;
        if (distanceFromPlayer > 50) {
            distanceFromPlayer = 50;
        }
        if (distanceFromPlayer < 10) {
            distanceFromPlayer = 10;
        }
    }

    /**
     * Upravení pitch při držení pravého myšítka a pohybu myši
     */
    private void calculatePitch() {
        if (MouseButtonHandler.isButtonDown(1)) {
            float pitchChange = MouseCursorHandler.getDY() * 0.1f;
            pitch -= pitchChange;
        }
    }

    /**
     * Upravení angleAroundPlayer při držení levého myšítka a pohybu myší
     */
    private void calculateAngleAroundPlayer() {
        if (MouseButtonHandler.isButtonDown(0)) {
            float angleChange = MouseCursorHandler.getDX() * 0.1f;
            angleAroundPlayer += angleChange;
        }
    }

    /**
     * Vrací vektor pozice
     *
     * @return Vector3f
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * Vrací natočení podle osy X
     *
     * @return float
     */
    public float getPitch() {
        return pitch;
    }

    /**
     * Vrací natočení podle osy Y
     *
     * @return float
     */
    public float getYaw() {
        return yaw;
    }
}
