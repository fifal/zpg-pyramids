package cz.filek.zpg.entities;

import cz.filek.zpg.engine.DisplayManager;
import cz.filek.zpg.input.KeyboardHandler;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.TexturedModel;
import cz.filek.zpg.terrains.Terrain;


import static org.lwjgl.glfw.GLFW.*;

/**
 * @author Filip Jani - 6. 11. 2016.
 *         <p>
 *         Třída sloužící k uchovávání informací o hráči a různé výpočty pro pohyb
 *         Zase obsahuje magickou konstantu stejně jako u Camera
 */
public class Player extends Entity {
    // Rychlost hráče
    private static final float RUN_SPEED = 50;
    // Rychlost zatáčení
    private static final float TURN_SPEED = 160;
    // Magická konstanta
    private static final float MAGIC_CONSTANT = 58.5f;
    // Gravitace
    private static final float GRAVITY = -50;


    private float currentSpeed = 0; // Aktuální rychlost
    private float currentTurnSpeed = 0; // Aktuální rychlost zatáčení
    private float upwardsSpeed = 0; // Skok

    /**
     * Konstruktor
     *
     * @param texturedModel Otexturovaný model
     * @param position      Vector3f pozice
     * @param rotX          rotace okolo osy X
     * @param rotY          rotace okolo osy Y
     * @param rotZ          rotace okolo osy Z
     * @param scale         škálování
     */
    public Player(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        super(texturedModel, position, rotX, rotY, rotZ, scale);
    }

    /**
     * Výpočet pohybu hráče podle stisknutých kláves
     *
     * @param terrain Terrain pro výpočet výšky
     */
    public void move(Terrain terrain) {
        checkInputs();
        super.increaseRotation(0, currentTurnSpeed * DisplayManager.getFrameTimeSeconds() * MAGIC_CONSTANT, 0);

        float distance = currentSpeed * DisplayManager.getFrameTimeSeconds();
        float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotY()) / MAGIC_CONSTANT));
        float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotY()) / MAGIC_CONSTANT));
        super.increasePosition(dx, 0, dz);

        upwardsSpeed += GRAVITY * DisplayManager.getFrameTimeSeconds();
        super.increasePosition(0, upwardsSpeed * DisplayManager.getFrameTimeSeconds(), 0);
        float height = terrain.getHeightOfTerrain(super.getPosition().x, super.getPosition().z);
        if (super.getPosition().y < height) {
            upwardsSpeed = 0;
            super.getPosition().y = height;
        }
    }

    /**
     * Nastavení proměnných podle stisknutých kláves
     */
    private void checkInputs() {
        if (KeyboardHandler.isKeyDown(GLFW_KEY_W)) {
            this.currentSpeed = RUN_SPEED;
        } else if (KeyboardHandler.isKeyDown(GLFW_KEY_S)) {
            this.currentSpeed = -RUN_SPEED;
        } else {
            this.currentSpeed = 0;
        }

        if (KeyboardHandler.isKeyDown(GLFW_KEY_D)) {
            this.currentTurnSpeed = -TURN_SPEED;
        } else if (KeyboardHandler.isKeyDown(GLFW_KEY_A)) {
            this.currentTurnSpeed = TURN_SPEED;
        } else {
            this.currentTurnSpeed = 0;
        }
    }
}
