package cz.filek.zpg.entities;

import cz.filek.zpg.math.Vector3f;

/**
 * @author Filip Jani - 4. 11. 2016.
 *         <p>
 *         Třída uchovávající informace o světle - pozice a barva
 */
public class Light {
    private Vector3f position;
    private Vector3f color;

    /**
     * Konstruktor
     *
     * @param position Vector3f pozice
     * @param color    Vector3f světlo
     */
    public Light(Vector3f position, Vector3f color) {
        this.position = position;
        this.color = color;
    }

    /**
     * Vrací vektor pozice
     *
     * @return Vector3f
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * Vrací vektor barvy světla
     *
     * @return Vector3f
     */
    public Vector3f getColor() {
        return color;
    }
}
