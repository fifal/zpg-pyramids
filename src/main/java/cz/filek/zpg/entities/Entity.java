package cz.filek.zpg.entities;

import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.TexturedModel;

/**
 * @author Filip Jani - 1. 11. 2016.
 *         <p>
 *         Třída uchovávající informace o jednotlivých entitách
 */
public class Entity {
    // TexturedModel což je RawModel + ModelTexture
    private TexturedModel texturedModel;
    private Vector3f position;
    private float rotX, rotY, rotZ;
    private float scale;

    /**
     * Konstruktor
     *
     * @param texturedModel Otexturovaný model
     * @param position      pozice ve scéně
     * @param rotX          rotace okolo osy X
     * @param rotY          rotace okolo osy Y
     * @param rotZ          rotace okolo osy Z
     * @param scale         škálování
     */
    public Entity(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        this.texturedModel = texturedModel;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;
    }

    /**
     * Úprava pozice
     * - zároveń řeší kolizi s hranicemi scény
     *
     * @param dx rozdíl na ose X
     * @param dy rozdíl na ose Y
     * @param dz rozdíl na ose Z
     */
    public void increasePosition(float dx, float dy, float dz) {
        this.position.x += dx;
        if (this.position.x > 290) {
            this.position.x -= dx;
        } else if (this.position.x < -290) {
            this.position.x += -dx;
        }
        this.position.y += dy;
        this.position.z += dz;
        if (this.position.z > 290) {
            this.position.z -= dz;
        } else if (this.position.z < -290) {
            this.position.z += -dz;
        }
    }

    /**
     * Úprava rotace
     * - kvůli špatným výpočtům transformačních matic je zde podmínka, pokud je float s rotací moc velký
     * auto začíná mizet mimo kameru
     *
     * @param dx rozdíl rotace na ose X
     * @param dy rozdíl rotace na ose Y
     * @param dz rozdíl rotace na ose Z
     */
    public void increaseRotation(float dx, float dy, float dz) {
        this.rotX += dx;
        this.rotY += dy;
        if (this.rotY < -12027) {
            this.rotY = 8190;
        } else if (this.rotY > 28126) {
            this.rotY = 8190;
        }
        this.rotZ += dz;
    }

    /**
     * Vrací TexturedModel
     *
     * @return TexturedModel
     */
    public TexturedModel getTexturedModel() {
        return texturedModel;
    }

    /**
     * Vrací vektor pozice
     *
     * @return Vector3f
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * Nastavení pozice
     *
     * @param position Vector3f
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * Vrací rotaci okolo osy X
     *
     * @return float
     */
    public float getRotX() {
        return rotX;
    }

    /**
     * Vrací rotaci okolo osy Y
     *
     * @return float
     */
    public float getRotY() {
        return rotY;
    }

    /**
     * Vrací rotaci okolo osy Z
     *
     * @return float
     */
    public float getRotZ() {
        return rotZ;
    }

    /**
     * Vrací škálování
     *
     * @return float
     */
    public float getScale() {
        return scale;
    }
}
