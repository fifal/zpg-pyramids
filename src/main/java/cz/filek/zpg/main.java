package cz.filek.zpg;

import cz.filek.zpg.engine.*;
import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.entities.Entity;
import cz.filek.zpg.entities.Light;
import cz.filek.zpg.entities.Player;
import cz.filek.zpg.input.KeyboardHandler;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.RawModel;

import static org.lwjgl.glfw.GLFW.*;

import cz.filek.zpg.models.TexturedModel;
import cz.filek.zpg.terrains.Terrain;
import cz.filek.zpg.textures.ModelTexture;
import cz.filek.zpg.textures.TerrainTexture;
import cz.filek.zpg.textures.TerrainTexturePack;
import cz.filek.zpg.utils.FPSCounter;
import cz.filek.zpg.utils.MousePicker;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Filip Jani -  1. 11. 2016.
 *         Hlavní třída zajišťující vytvoření objektů a vykreslení
 */
public class main {
    private static long windowID;

    public static void main(String[] args) {
        windowID = DisplayManager.createDisplay();

        Loader loader = new Loader();

        // MODELS ########################################################################################
        RawModel grassModel = OBJLoader.loadObjModel("terrain/grassModel", loader);
        ModelTexture grassTexture = new ModelTexture(loader.loadTexture("terrain/desertFlower"));
        TexturedModel grassTextModel = new TexturedModel(grassModel, grassTexture);

        grassTextModel.getModelTexture().setHasTransparency(true);
        grassTextModel.getModelTexture().setUseFakeLighting(true);

        RawModel pyramidModel = OBJLoader.loadObjModel("pyramida2", loader);
        ModelTexture pyramidTexture = new ModelTexture(loader.loadTexture("pyramida2"));
        TexturedModel pyramidTextModel = new TexturedModel(pyramidModel, pyramidTexture);

        RawModel pyramidModel2 = OBJLoader.loadObjModel("pyramida3", loader);
        ModelTexture pyramidTexture2 = new ModelTexture(loader.loadTexture("pyramida3"));
        TexturedModel pyramidTextModel2 = new TexturedModel(pyramidModel2, pyramidTexture2);

        RawModel pyramidModel3 = OBJLoader.loadObjModel("pyramida", loader);
        ModelTexture pyramidTexture3 = new ModelTexture(loader.loadTexture("pyramida"));
        TexturedModel pyramidTextModel3 = new TexturedModel(pyramidModel3, pyramidTexture3);

        RawModel tree = OBJLoader.loadObjModel("treeSmrk", loader);
        ModelTexture treeModel = new ModelTexture(loader.loadTexture("treeSmrk"));
        TexturedModel treeTextModel = new TexturedModel(tree, treeModel);
        // ##############################################################################################

        // TERÉN ########################################################################################
        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("terrain/blend/sand"));
        TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("terrain/blend/rock"));
        TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("terrain/blend/grassText"));
        TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("terrain/blend/sandTrail"));
        TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("terrain/blend/blendMap"));

        TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);

        BufferedImage heightMap = null;
        try {
            InputStream inputStream = main.class.getResourceAsStream("/textures/terrain/heightmap1.png");
            heightMap = ImageIO.read(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Terrain terrain = new Terrain(-0.5f, -0.5f, loader, texturePack, blendMap, heightMap);
        // ##############################################################################################

        // POUŠTNÍ TRÁVA ################################################################################
        Random random = new Random();
        List<Entity> entities = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            float lower = -512f;
            float upper = 512f;
            float x = getRandomRange(lower, upper);
            float z = getRandomRange(lower, upper);
            entities.add(new Entity(grassTextModel, new Vector3f(x, terrain.getHeightOfTerrain(x, z), z), 0, 0, 0, 2));
        }
        // ##############################################################################################

        Light light = new Light(new Vector3f(-245, 800, 500), new Vector3f(1, 1, 1));

        // HRÁČ #########################################################################################
        RawModel playerModel = OBJLoader.loadObjModel("car", loader);
        TexturedModel playerTextModel = new TexturedModel(playerModel, new ModelTexture(loader.loadTexture("car")));
        Player player = new Player(playerTextModel, new Vector3f(250, 0, -100), 0, 140 * 58.5f, 0, 2);
        // ##############################################################################################

        Camera camera = new Camera(terrain, player);

        // PYRAMIDY #####################################################################################
        Entity pyramidEntity = new Entity(pyramidTextModel, new Vector3f(150, -27f, -80), 0, -220 * 58.5f, 0, 50);
        Entity pyramidEntity2 = new Entity(pyramidTextModel2, new Vector3f(-100, -24f, -7), 0, -300 * 58.5f, 0, 6);
        Entity pyramidEntity3 = new Entity(pyramidTextModel3, new Vector3f(-57, -23f, 242), 0, -300 * 58.5f, 0, 25);
        // ##############################################################################################

        // OAZY #########################################################################################
        for (int i = 0; i < 100; i++) {
            float x = getRandomRange(-282, 266);
            float z = getRandomRange(161, 249);
            while (x < -16 && x > -93) {
                x = getRandomRange(-282, 266);
            }
            while (z > 207 && z < 274) {
                z = getRandomRange(161, 249);
            }
            float y = terrain.getHeightOfTerrain(x, z);
            float scale = getRandomRange(2, 6);
            Entity ent = new Entity(treeTextModel, new Vector3f(x, y, z), 0, 0, 0, scale);
            entities.add(ent);
        }
        for (int i = 0; i < 50; i++) {
            float x = getRandomRange(-107, -281);
            float z = getRandomRange(-276, -130);
            float y = terrain.getHeightOfTerrain(x, z);
            float scale = getRandomRange(2, 6);
            Entity ent = new Entity(treeTextModel, new Vector3f(x, y, z), 0, 0, 0, scale);
            entities.add(ent);
        }
        // ##############################################################################################

        MainRenderer renderer = new MainRenderer(loader);
        MousePicker picker = new MousePicker(camera, renderer.getProjectionMatrix(), renderer);

        while (!glfwWindowShouldClose(windowID)) {
            FPSCounter.StartCounter();
            if (KeyboardHandler.isKeyDown(GLFW_KEY_ESCAPE)) {
                break;
            }
            DisplayManager.sync(100);

            renderer.render(light, camera);

            // Vykreslení všech entit - stromy, květiny
            for (Entity ent : entities) {
                renderer.processEntity(ent);
            }

            // Vykreslení hráče
            renderer.processEntity(player);

            // Zachytávání pohybů kamery a hráče
            camera.move();
            player.move(terrain);
            picker.update();

            // Vykreslení pyramid
            renderer.processEntity(pyramidEntity);
            renderer.processEntity(pyramidEntity2);
            renderer.processEntity(pyramidEntity3);
            renderer.processTerrain(terrain);

            // Vypsání FPS do titulku okna
            long fps = FPSCounter.StopAndPost();
            if (fps != 0) {
                glfwSetWindowTitle(windowID, "ZPG - FPS: " + fps);
            }

            glfwSwapBuffers(windowID);
            glfwPollEvents();
        }

        // Uklízení
        loader.cleanUp();
        renderer.cleanUp();

        glfwDestroyWindow(windowID);
        glfwTerminate();
    }

    /**
     * Vrací náhodný float z rozsahu <min - max>
     *
     * @param min float
     * @param max float
     * @return float
     */
    public static float getRandomRange(float min, float max) {
        float lower = min;
        float upper = max;
        float result = (float) (Math.random() * (upper - lower) + lower);
        return result;
    }

}
