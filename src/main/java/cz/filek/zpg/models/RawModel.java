package cz.filek.zpg.models;

/**
 * @author Filip Jani - 1. 11. 2016.
 *         <p>
 *         Třída ukládající VAO modelu, počet vrcholů a šířku a délku modelu
 */
public class RawModel {
    private int vaoID;
    private int vertexCount;
    private float width;
    private float height;

    /**
     * Konstruktor
     *
     * @param vaoID       ID VAO
     * @param vertexCount počet vrcholů
     * @param width       šířka
     * @param height      délka
     */
    public RawModel(int vaoID, int vertexCount, float width, float height) {
        this.vaoID = vaoID;
        this.vertexCount = vertexCount;
        this.width = width;
        this.height = height;
    }

    /**
     * Vrací ID VAO
     *
     * @return int
     */
    public int getVaoID() {
        return vaoID;
    }

    /**
     * Vrací počet vrcholů
     *
     * @return int
     */
    public int getVertexCount() {
        return vertexCount;
    }
}
