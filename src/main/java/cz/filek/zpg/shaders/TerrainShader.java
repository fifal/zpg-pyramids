package cz.filek.zpg.shaders;


import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.entities.Light;
import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Matrix4f;
import cz.filek.zpg.math.Vector3f;

/**
 * @author Filip Jani -  5. 11. 2016.
 *         Shader pro Terén
 */
public class TerrainShader extends ShaderProgram {
    private static final String VERTEX_FILE = "/shaders/terrainVertexShader.vs";
    private static final String FRAGMENT_FILE = "/shaders/terrainFragmentShader.vs";

    private int locTransMatrix;
    private int locProjMatrix;
    private int locViewMatrix;
    private int locLightPosition;
    private int locLightColor;
    private int locSkyColor;
    private int locBgTexture;
    private int locRTexture;
    private int locGTexture;
    private int locBTexture;
    private int locBlendMap;

    public TerrainShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void getAllUniformLocations() {
        locTransMatrix = super.getUniformLocation("transMatrix");
        locProjMatrix = super.getUniformLocation("projMatrix");
        locViewMatrix = super.getUniformLocation("viewMatrix");
        locLightColor = super.getUniformLocation("lightColor");
        locLightPosition = super.getUniformLocation("lightPosition");
        locSkyColor = super.getUniformLocation("skyColor");

        locBgTexture = super.getUniformLocation("backgroundTexture");
        locRTexture = super.getUniformLocation("rTexture");
        locGTexture = super.getUniformLocation("gTexture");
        locBTexture = super.getUniformLocation("bTexture");
        locBlendMap = super.getUniformLocation("blendMap");

    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    public void loadTransmatrix(Matrix4f matrix4f) {
        super.loadMatrix(locTransMatrix, matrix4f);
    }

    public void loadProjMatrix(Matrix4f projection) {
        super.loadMatrix(locProjMatrix, projection);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        super.loadMatrix(locViewMatrix, viewMatrix);
    }

    public void loadLight(Light light) {
        super.loadVector(locLightColor, light.getColor());
        super.loadVector(locLightPosition, light.getPosition());
    }

    public void loadSkyColor(float r, float g, float b) {
        super.loadVector(locSkyColor, new Vector3f(r, g, b));
    }

    public void connectTextureUnits() {
        super.loadInt(locBgTexture, 0);
        super.loadInt(locRTexture, 1);
        super.loadInt(locGTexture, 2);
        super.loadInt(locBTexture, 3);
        super.loadInt(locBlendMap, 4);
    }
}
