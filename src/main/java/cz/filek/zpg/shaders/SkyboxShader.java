package cz.filek.zpg.shaders;

import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Matrix4f;

/**
 * @author Filip Jani - 22. 11. 2016.
 *         <p>
 *         Shader pro skybox
 */
public class SkyboxShader extends ShaderProgram {
    private static final String VERTEX_FILE = "/shaders/skyboxVertexShader.txt";
    private static final String FRAGMENT_FILE = "/shaders/skyboxFragmentShader.txt";

    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_transMatrix;

    public SkyboxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(location_projectionMatrix, matrix);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        matrix.m30 = 0;
        matrix.m31 = 0;
        matrix.m32 = 0;
        super.loadMatrix(location_viewMatrix, matrix);
    }

    public void loadTransMatrix(Matrix4f matrix4f) {
        super.loadMatrix(location_transMatrix, matrix4f);
    }

    @Override
    protected void getAllUniformLocations() {
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        location_transMatrix = super.getUniformLocation("transMatrix");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
}
