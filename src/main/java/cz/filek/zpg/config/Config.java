package cz.filek.zpg.config;

/**
 * @author Filip Jani - 1. 11. 2016.
 */
public class Config {
    // Nastavení velikosti okna
    public static int APP_WIDTH = 1280;
    public static int APP_HEIGHT = 720;
}
