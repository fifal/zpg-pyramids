package cz.filek.zpg.utils;

import cz.filek.zpg.config.Config;
import cz.filek.zpg.engine.MainRenderer;
import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.input.MouseCursorHandler;
import cz.filek.zpg.math.*;

/**
 * @author Filip Jani - 13. 11. 2016
 *         Metoda sloužící k zachytávání pozice myši ve 3D světě
 *         - Nemá využití, ale protože používá funkce pro práci s kamerou musí zde být i když je nevyužita,
 *           jinak by se kamera nezobrazovala správně
 */
public class MousePicker {
    private Vector3f currentRay;

    private MainRenderer renderer;

    private Matrix4f projectionMatrix;
    private Matrix4f viewMatrix;
    private Camera camera;

    public MousePicker(Camera camera, Matrix4f projectionMatrix, MainRenderer renderer) {
        this.camera = camera;
        this.projectionMatrix = projectionMatrix;
        this.viewMatrix = Maths.createViewMatrix(camera);
        this.renderer = renderer;
    }

    public Vector3f getCurrentRay() {
        return currentRay;
    }

    public void update() {
        viewMatrix = Maths.createViewMatrix(camera);
        currentRay = calculateMouseRay();
//        if (MouseButtonHandler.isButtonDown(0)) {
//            Entity entity = new Entity(treeTextModel, getPointOnRay(currentRay, 120), 0, 0, 0, 4);
//            renderer.processEntity(entity);
//        }
    }

    private Vector3f calculateMouseRay() {
        float mouseX = MouseCursorHandler.getPosition().x;
        float mouseY = MouseCursorHandler.getPosition().y;

        Vector2f normalized = getNormalizedDeviceCoords(mouseX, mouseY);
        Vector4f clipCoords = new Vector4f(normalized.x, normalized.y, -1f, 1f);
        Vector4f eyeCoords = toEyeCoords(clipCoords);
        Vector3f worldRay = toWorldCoords(eyeCoords);
        return worldRay;
    }

    private Vector3f toWorldCoords(Vector4f eyeCoords) {
        Matrix4f invertedView = Matrix4f.invert(viewMatrix, null);
        Vector4f rayWorld = Matrix4f.transform(invertedView, eyeCoords, null);
        Vector3f mouseRay = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
        //mouseRay.normalize();
        return mouseRay;
    }

    private Vector4f toEyeCoords(Vector4f clipCoords) {
        Matrix4f invertedProjection = Matrix4f.invert(projectionMatrix, null);
        Vector4f eyeCoords = Matrix4f.transform(invertedProjection, clipCoords, null);


        return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0f);
    }

    private Vector2f getNormalizedDeviceCoords(float mouseX, float mouseY) {
        float x = (2f * mouseX) / Config.APP_WIDTH - 1;
        float y = (2f * mouseY) / Config.APP_HEIGHT - 1f;

        return new Vector2f(x, -y);
    }

    private Vector3f getPointOnRay(Vector3f ray, float distance) {
//        Vector3f camPos = camera.getPosition();
//        Vector3f start = new Vector3f(camPos.x, camPos.y, camPos.z);
//        Vector3f scaledRay = new Vector3f(ray.x * distance, ray.y * distance, ray.z * distance);
//        return start.add(scaledRay);
        return new Vector3f();
    }
}
