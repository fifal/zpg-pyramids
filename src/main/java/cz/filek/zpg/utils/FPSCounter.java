package cz.filek.zpg.utils;

/**
 * @author Filip Jani - 6. 11. 2016.
 *         <p>
 *         Třída sloužící ke zjišťování počtu snímků za vteřinu
 */
final public class FPSCounter {
    private static int startTime;
    private static int endTime;
    private static int frameTimes = 0;
    private static short frames = 0;

    /**
     * Začátek počítání FPS
     **/
    public final static void StartCounter() {
        startTime = (int) System.currentTimeMillis();
    }

    /**
     * Zastavení počítání a vrácení výsledku
     */
    public final static long StopAndPost() {
        endTime = (int) System.currentTimeMillis();

        // Rozdíl mezi časy
        frameTimes = frameTimes + endTime - startTime;

        // Přičtení dalšího snímku
        ++frames;

        // Poukud je čas měření větší nebo roven jedné vteříně
        if (frameTimes >= 1000) {
            long result = frames;
            frames = 0;
            frameTimes = 0;
            return result;
        }
        return 0;
    }
}