package cz.filek.zpg.textures;

import cz.filek.zpg.engine.OBJLoader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.PNGDecoder;

import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * @author Filip Jani - 1. 11. 2016.
 *         <p>
 *         Třída pro nahrávání PNG textur ze souboru
 */
public class TextureLoader {
    /**
     * Načtení dané textury z resources/textures/
     *
     * @param filename název textury
     * @return TextureData Informace o textuře
     */
    public static TextureData loadPNGTexture(String filename) {
        ByteBuffer buf = null;
        int tWidth = 0;
        int tHeight = 0;

        try {
            String texturePath = "/textures/" + filename + ".png";
            InputStream inputStream = OBJLoader.class.getResourceAsStream(texturePath);
            PNGDecoder decoder = new PNGDecoder(inputStream);

            tWidth = decoder.getWidth();
            tHeight = decoder.getHeight();


            buf = ByteBuffer.allocateDirect(
                    4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.RGBA);
            buf.flip();

            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return new TextureData(tWidth, tHeight, buf);
    }

    /**
     * Bindování textury do OpenGL
     *
     * @param tWidth  šířka textury
     * @param tHeight výška textury
     * @param buf     ByteBuffer textury
     * @return vrací ID textury
     */
    public static int bindTexture(int tWidth, int tHeight, ByteBuffer buf) {
        // Create a new texture object in memory and bind it
        int texId = GL11.glGenTextures();
        GL13.glActiveTexture(GL11.GL_TEXTURE_2D);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);

        // All RGB bytes are aligned to each other and each component is 1 byte
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        // Upload the texture data and generate mip maps (for scaling)
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, tWidth, tHeight, 0,
                GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

        // Setup the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

        GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.4f);
        // Setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
                GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);

        return texId;
    }
}
