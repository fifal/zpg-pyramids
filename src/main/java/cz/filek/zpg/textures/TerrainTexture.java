package cz.filek.zpg.textures;

/**
 * @author Filip Jani -  6. 11. 2016.
 *         <p>
 *         Třída uchovávající ID textury terénu
 */
public class TerrainTexture {
    private int textureID;

    /**
     * Konstruktor
     *
     * @param textureID ID textury
     */
    public TerrainTexture(int textureID) {
        this.textureID = textureID;
    }

    /**
     * Vrací ID textury terénu
     *
     * @return int
     */
    public int getTextureID() {
        return textureID;
    }
}
