package cz.filek.zpg.textures;

/**
 * @author Filip Jani -  6. 11. 2016.
 *         <p>
 *         Uchovává všechny textury blend mapy pro terén
 */
public class TerrainTexturePack {
    private TerrainTexture backgroundTexture;
    private TerrainTexture rTexture;
    private TerrainTexture gTexture;
    private TerrainTexture bTexture;

    /**
     * Konstruktor
     *
     * @param backgroundTexture hlavní textura pozadí
     * @param rTexture          textura pro červenou složku blend mapy
     * @param gTexture          textura pro zelenou složku blend mapy
     * @param bTexture          textura pro modrou složku blend mapy
     */
    public TerrainTexturePack(TerrainTexture backgroundTexture, TerrainTexture rTexture, TerrainTexture gTexture, TerrainTexture bTexture) {
        this.backgroundTexture = backgroundTexture;
        this.rTexture = rTexture;
        this.gTexture = gTexture;
        this.bTexture = bTexture;
    }

    /**
     * Vrací hlavní texturu terénu
     *
     * @return TerrainTexture
     */
    public TerrainTexture getBackgroundTexture() {
        return backgroundTexture;
    }

    /**
     * Vrací texturu pro červenou složku terénu
     *
     * @return TerrainTexture
     */
    public TerrainTexture getrTexture() {
        return rTexture;
    }

    /**
     * Vrací texturu pro zelenou složku terénu
     *
     * @return TerrainTexture
     */
    public TerrainTexture getgTexture() {
        return gTexture;
    }

    /**
     * Vrací texturu pro modrou složku terénu
     *
     * @return TerrainTexture
     */
    public TerrainTexture getbTexture() {
        return bTexture;
    }

}
