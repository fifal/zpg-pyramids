package cz.filek.zpg.textures;

/**
 * @author Filip Jani -  1. 11. 2016.
 *         <p>
 *         Třída uchovávající informace o textuře
 */
public class ModelTexture {
    private int textureID;
    // Má textura průhlednost?
    private boolean hasTransparency = false;
    // Používáme falešné světlo?
    private boolean useFakeLighting = false;

    /**
     * Konstruktor
     *
     * @param textureID ID textury
     */
    public ModelTexture(int textureID) {
        this.textureID = textureID;
    }

    /**
     * Vrací ID textury
     *
     * @return int
     */
    public int getID() {
        return textureID;
    }

    /**
     * Vrací jestli má textura průhlednost
     *
     * @return boolean
     */
    public boolean hasTransparency() {
        return hasTransparency;
    }

    /**
     * Nastavení průhlednosti
     *
     * @param hasTransparency boolean
     */
    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }

    /**
     * Vrací jestli používáme falešné světlo
     *
     * @return boolean
     */
    public boolean useFakeLighting() {
        return useFakeLighting;
    }

    /**
     * Nastavení používání falešného světla
     *
     * @param useFakeLighting boolean
     */
    public void setUseFakeLighting(boolean useFakeLighting) {
        this.useFakeLighting = useFakeLighting;
    }
}
