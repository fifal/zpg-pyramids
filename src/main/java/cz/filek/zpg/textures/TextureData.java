package cz.filek.zpg.textures;

import java.nio.ByteBuffer;

/**
 * @author Filip Jani - 22. 11. 2016.
 *         <p>
 *         Třída uchovávající informace o texturře pro GL_TEXTURE_CUBE_MAP
 */
public class TextureData {
    private int width;
    private int height;
    private ByteBuffer buffer;

    /**
     * Konstruktor
     *
     * @param width  šířka textury
     * @param height výška textury
     * @param buffer ByteBuffer
     */
    public TextureData(int width, int height, ByteBuffer buffer) {
        this.width = width;
        this.height = height;
        this.buffer = buffer;
    }

    /**
     * Vrací šířku textury
     *
     * @return int
     */
    public int getWidth() {
        return width;
    }

    /**
     * Vrací výšku textury
     *
     * @return itn
     */
    public int getHeight() {
        return height;
    }

    /**
     * Vrací ByteBuffer textury
     *
     * @return ByteBuffer
     */
    public ByteBuffer getBuffer() {
        return buffer;
    }
}
