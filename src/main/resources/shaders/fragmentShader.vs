#version 400 core

in vec2 passTextureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector;
in float visibility;

out vec4 outColor;

uniform sampler2D textureSampler;
uniform vec3 lightColor;
uniform vec3 skyColor;

const float levels = 2048;

void main(void){
    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitLightVector = normalize(toLightVector);

    float nDotl = dot(unitNormal, unitLightVector);
    float brightness = max(nDotl, 0.2);
    float level = floor(brightness * levels);
    brightness = level / levels;
    vec3 diffuse = brightness * lightColor;
    vec4 textureColor = texture(textureSampler, passTextureCoords);
    if(textureColor.a < 0.5){
        discard;
    }

    outColor = vec4(diffuse,1.0) * textureColor;
    outColor = mix(vec4(skyColor, 1.0), outColor, visibility);
}